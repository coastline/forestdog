﻿using System;
using System.Linq;
using System.Reflection;

namespace Events
{
    public struct Types
    {
        public const string TimeReversalPowerUpEnable = "time-reversal-powerup-enable";
        public const string TimeReversalPowerUpDisable = "time-reversal-powerup-disable";

        public const string InteractableDetected = "interactable-detected-event";
        public const string InteractableRemoved = "interactable-removed-event";

        /**
         * Contains all the events defined as public constant strings
         */
        internal static readonly string[] Events = GetEventNames();

        /**
         * Returns the all the events defined as public constant strings in the Types struct
         */
        private static string[] GetEventNames()
        {
            return (
                from fi in typeof(Types).GetFields(BindingFlags.Public | BindingFlags.Static)
                where fi.IsLiteral && !fi.IsInitOnly && Type.GetTypeCode(fi.GetValue(null).GetType()) == TypeCode.String
                select fi.GetValue(null).ToString()
            ).ToArray();
        }
    }
}