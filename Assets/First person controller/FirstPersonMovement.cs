﻿using Player;
using UnityEngine;
using FMODUnity;

public class FirstPersonMovement : MonoBehaviour
{
    public PlayerData playerData;

    private Vector2 velocity;
    private bool previousIsMoving;

    [FMODUnity.EventRef]
    public string FmodEvent = "";
    FMOD.Studio.EventInstance playerFootSteps;
    

    void FixedUpdate()
    {
        velocity.y = Input.GetAxis("Vertical") * playerData.speed * Time.fixedDeltaTime;
        velocity.x = Input.GetAxis("Horizontal") * playerData.speed * Time.fixedDeltaTime;

        bool isMoving = velocity.magnitude > 3e-5;
        if (isMoving != previousIsMoving) {
            if (isMoving) { // if started moving
                PlayerStepsStart();
            } else { // else, if it stopped moving
                PlayerStepsStop();
            }
        }

        transform.Translate(velocity.x, 0, velocity.y);
        previousIsMoving = isMoving;
    }

    private void PlayerStepsStart()
    {
        playerFootSteps = FMODUnity.RuntimeManager.CreateInstance(FmodEvent);
        playerFootSteps.start();
    }

    private void PlayerStepsStop()
    {
        playerFootSteps.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }

}
