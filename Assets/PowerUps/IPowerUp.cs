namespace PowerUps
{
    public interface IPowerUp
    {
        bool IsEnabled();
        void Enable();
        void Disable();
    }
}