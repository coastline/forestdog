using Interactable;
using Interactable.Gramophone;
using UnityEngine;

namespace PowerUps
{
    public class PowerUpEnabler : MonoBehaviour, IInteractable
    {
        private IPowerUp powerUp;
        public GramphoneMusicPlayer2 player;

        private void Start()
        {
            powerUp = GetComponent<IPowerUp>() ?? GetComponentInChildren<IPowerUp>();
            if (ReferenceEquals(powerUp, null)) {
                Debug.LogError("game object needs to have a component that implement IPowerUp interface");
                enabled = false;
                return;
            }
        }

        public void StartInteracting()
        {
            if (!powerUp.IsEnabled()) {
                powerUp.Enable();
                player.PlaySong();
            }
        }
        
        public void StopInteracting() { }
    }
}