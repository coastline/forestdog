using Interactable.Boulder;
using Player;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace PowerUps
{
    public class BoulderPickUpPowerUp : MonoBehaviour, IPowerUp
    {
        public TargetPositionFollower follower;
        public PlayerData playerData;
        public float playerSpeedMultiplier = 0.5f;

        private float _originalPlayerSpeed;
        private GameObject _player;
        private bool _isEnabled;

        public bool IsEnabled()
        {
            return _isEnabled;
        }

        public void Enable()
        {
            if (ReferenceEquals(follower, null)) {
                Debug.LogError("failed to enable boulder following behaviour because it does not exist");
                return;
            }

            follower.SetTarget(_player);
            playerData.speed *= playerSpeedMultiplier;
            _isEnabled = true;
        }

        public void Disable()
        {
            if (ReferenceEquals(follower, null)) {
                Debug.LogError("failed to disable boulder following behaviour because it does not exist");
                return;
            }

            follower.SetTarget(null);
            playerData.speed = _originalPlayerSpeed;
            _isEnabled = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.gameObject.CompareTag("Player")) return;

            _player = other.gameObject;
        }

        private void OnTriggerExit(Collider other)
        {
            if (!other.gameObject.CompareTag("Player")) return;

            if (_isEnabled) {
                follower.SetTarget(null);
                playerData.speed = _originalPlayerSpeed;
                _isEnabled = false;
            }

            _player = null;
        }

        private void Start()
        {
            _originalPlayerSpeed = playerData.speed;
        }

        private void OnDisable()
        {
            playerData.speed = _originalPlayerSpeed;
        }
    }
}