using Interactable;
using UnityEngine;
using FMODUnity;

namespace PowerUps
{
    public class PowerUpToggler : MonoBehaviour, IInteractable
    {
        private IPowerUp powerUp;

        [FMODUnity.EventRef]
        public string FmodEvent = "";
        FMOD.Studio.EventInstance boulderPush;

        private void Start()
        {
            powerUp = GetComponent<IPowerUp>() ?? GetComponentInChildren<IPowerUp>();
            if (ReferenceEquals(powerUp, null)) {
                Debug.LogError("game object needs to have a component that implement IPowerUp interface");
                enabled = false;
                return;
            }
        }

        public void StartInteracting()
        {
            if (!powerUp.IsEnabled())
            {
                powerUp.Enable();
                boulderPush = FMODUnity.RuntimeManager.CreateInstance(FmodEvent);
                boulderPush.start();
            }
        }

        public void StopInteracting()
        {
            if (powerUp.IsEnabled()) 
            {
                powerUp.Disable();
                boulderPush.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            }
        }
    }
}