using Events;
using UnityEngine;

namespace PowerUps
{
    public class TimeReversalPowerUp : MonoBehaviour, IPowerUp
    {
        private bool _isEnabled;

        public bool IsEnabled()
        {
            return _isEnabled;
        }

        public void Enable()
        {
            SimpleEventSystem.TriggerEvent(Events.Types.TimeReversalPowerUpEnable);
            _isEnabled = true;
        }

        public void Disable()
        {
            SimpleEventSystem.TriggerEvent(Events.Types.TimeReversalPowerUpDisable);
            _isEnabled = false;
        }
    }
}