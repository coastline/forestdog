using System;
using Interactable.Gramophone;
using UnityEngine;

namespace PowerUps
{
    public class PowerUpDisablerOnPlayerExit : MonoBehaviour
    {
        private IPowerUp powerUp;
        public GramphoneMusicPlayer2 player;


        private void Start()
        {
            powerUp = GetComponent<IPowerUp>() ?? GetComponentInChildren<IPowerUp>();
            if (ReferenceEquals(powerUp, null)) {
                Debug.LogError("game object needs to have a component that implement IPowerUp interface");
                enabled = false;
                return;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (!other.gameObject.tag.Equals("Player")) return;

            powerUp.Disable();
            player.StopSong();
        }
    }
}