﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class End : MonoBehaviour
{

    private SphereCollider SphereColliderEND;


    // Start is called before the first frame update
    void Start()
    {
        SphereColliderEND = GetComponent<SphereCollider>();
    }

    // Update is called once per frame
    void Update()
    {

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            WhiteFlashTimeTravel.PlayFadeToBlackBool = true;
            
        }
    }



}
