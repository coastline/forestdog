﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Safezone : MonoBehaviour
{
    public static bool IsInSaveZone { get; private set; }
    public SphereCollider SphereCollider { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        SphereCollider = GetComponent<SphereCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            IsInSaveZone = true;
            Debug.Log("Player Entered");
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            IsInSaveZone = false;
            Debug.Log("Player Left");
        }
    }

    public void ToggleSafeZone(bool on)
    {
       IsInSaveZone = on;
    }


}
