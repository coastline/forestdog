﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class FogSystem : MonoBehaviour
{

    private GameObject Player { get; set; }
    private CapsuleCollider PlayerCollider { get; set; }
    private PlayerState PlayerState { get; set; }
    private GameObject FogCapsule { get; set; }
    private FogPlayerScript FogPlayerScript { get; set; }
    public float Speed { get; set; } = 0.1f;
    private float secondsPassed = 0f;
    public new ParticleSystem particleSystem;
    private SphereCollider FogCollider { get; set; }
    private GameObject LightObject { get; set; }
    private Light Light { get; set; }
    private readonly float fogIncrease = 0.1f;
    private readonly float fogBase = 0.8f;
    private float curFog = 0.0f;
    private readonly float lightIncrease = 0.1f;
    private readonly float lightBase = 0.4f;
    private float curLight;
    private ParticleSystemRenderer ParticleRenderer { get; set; }
    private Vector3 offset;
    private readonly float distance = 60;
    private float ResetCounter { get; set; } = 0f;
    private GameObject ResetPoint { get; set; }
    private BoxCollider FogColliderBox { get; set; }
    private GameObject Fog { get; set; }

    private void Awake()
    {
        InvokeRepeating("SpeedUp", 1.0f, 1.0f);
    }


    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        PlayerState = Player.GetComponent<PlayerState>();
        FogCapsule = GameObject.FindGameObjectWithTag("CameraFog");
        FogPlayerScript = FogCapsule.GetComponent<FogPlayerScript>();
        Fog = GameObject.FindGameObjectWithTag("Fog System");
        particleSystem = Fog.GetComponent<ParticleSystem>();
        FogCollider = GetComponent<SphereCollider>();
        LightObject = GameObject.FindGameObjectWithTag("Light");
        Light = LightObject.GetComponent<Light>();
        
        curLight = Light.intensity;
        ParticleRenderer = particleSystem.GetComponent<ParticleSystemRenderer>();
        offset = transform.position - Player.transform.position;
        ResetPoint = GameObject.FindGameObjectWithTag("Teleport");
    }

    // Update is called once per frame
    void Update()
    {
        if(ParticleRenderer == null)
        {
            ParticleRenderer = particleSystem.GetComponent<ParticleSystemRenderer>();
            
        }
        if (ResetPoint == null)
        {
            ResetPoint = GameObject.FindGameObjectWithTag("Teleport");
        }

        var em = particleSystem.emission;
        if (Safezone.IsInSaveZone)
        {
            //transform.position = Player.transform.position + Player.transform.rotation.eulerAngles. new Vector3(-200, 0, 0);
            FogPlayerScript.Alpha = 0;
            ParticleRenderer.enabled = false;
            em.enabled = false;
            return;
        }
        else
        {
            
            if (ParticleRenderer.enabled == false)
            {
                ParticleRenderer.enabled = true;
            }
            transform.parent = null;
            transform.rotation = Quaternion.Euler(270, Player.transform.rotation.eulerAngles.y +90, 0);
            //transform.LookAt(Player.transform, Vector3.forward);
            em.enabled = true;
            transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, Speed * Time.deltaTime);
            
            float distancePercentage = (((Vector3.Distance((FogCollider.transform.position), Player.transform.position))) - FogCollider.radius) / 100;
            Debug.Log(distancePercentage);
            if (distancePercentage <= 0.3f )
            {
                if (curFog < fogBase)
                {
                    curFog += fogIncrease * Time.deltaTime;
                    FogPlayerScript.Alpha = curFog -distancePercentage;

                    if(curLight > lightBase)
                    {
                        curLight -= lightIncrease * Time.deltaTime;
                        Light.intensity = curLight;
                    }
                }    
            }
            else
            {
                if(curFog > 0)
                {
                    curFog -= fogIncrease * Time.deltaTime;
                    FogPlayerScript.Alpha = curFog - distancePercentage;
                    if (curLight < 1)
                    {
                        curLight += lightIncrease * Time.deltaTime;
                        Light.intensity = curLight;
                    }
                }
                

            }

           // Debug.Log(Vector3.Distance((transform.position), Player.transform.position));

      
        }
    }

    private void LateUpdate()
    {
        if (Safezone.IsInSaveZone)
        {
            //it was that simple....
            transform.position = Player.transform.position + -(Player.transform.forward) *distance;
        }
        
    }

    private void OnParticleCollision(GameObject other)
    {
        if (other.tag == "Player")
        {
            PlayerState.Reset = true;
            Debug.Log("Colling");
        }
    }

    void SpeedUp()
    {
        if (Safezone.IsInSaveZone)
        {
            Speed = 1f;
            secondsPassed = 0f;
        }
        else
        {
            //50 seconds = 6 speed
            Speed = 1f + secondsPassed / 5f;


            secondsPassed++;
        }
        if (PlayerState.Reset)
        {
            if (ResetCounter >= 5f)
            {
                Player.transform.position = ResetPoint.transform.position;
                ResetCounter = 0;
                FogPlayerScript.Alpha = 0;
                WrongDog.IsBadDog = false;
                PlayerState.Reset = false;
            }
            else
            {
                ResetCounter++;
            }
            
        }



            //Debug.Log(Vector3.Distance((FogCollider.transform.position), Player.transform.position) - FogCollider.radius);
            Debug.Log("Reset " + PlayerState.Reset);
            Debug.Log("Speed " + Speed + " Time " + secondsPassed);
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            PlayerState.Reset = true;
        }
    }





    //private Vector3 BullShitCalculation(float y)
    //{
    //    //screw this
    //    //        the logic is as follows
    //    //distance = distance away from player desired = 200

    //    //if (y between 0 and 90) 
    //    //z = distance - (distance * (between / -90))
    //    //x = distance * (between / 90)

    //    //if (y between 0 and - 90) 
    //    //z = distance - (distance * (between / 90))
    //    //x = -distance * (between / 90)

    //    //if (y between - 90 and - 180) 
    //    //z = -distance * ((between + 90) / 90)
    //    //x = -distance + (distance * ((between + 90) / 90))
    //    //FloofToday at 2:20 AM
    //    //if (y between 90 and 180)
    //    //z = distance * ((between - 90) / 90)
    //    //x = distance - (distance * ((between - 90) / 90))

    //    float z = 0;
    //    float x = 0;

    //    if(y >= 0 && y <= 90)
    //    {
    //        x = distance - (distance * (y / -90));
    //        z = distance * (y / 90);
    //    }
    //    else if (y >= 90 && y <= 180)
    //    {
    //        x = distance * ((y - 90) / 90);
    //        z = distance - (distance * ((y - 90) / 90));
    //    }


    //    if(y < 0)
    //    {
    //        y = y - 360;

    //        if (y <= 0 && y <= -90)
    //        {
    //            x = distance - (distance * (y / 90));
    //            z = -distance * (y / 90);
    //        }
    //        else if (y <= -90 && y <= -180)
    //        {
    //            x = -distance * ((y + 90) / 90);
    //            z = -distance * (distance * ((y + 90) / 90));
    //        }
    //    }






    //    return new Vector3(x, 0, z);
    //}

}