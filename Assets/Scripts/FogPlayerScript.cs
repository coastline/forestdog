﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogPlayerScript : MonoBehaviour
{
    [SerializeField]
    private Renderer fog;

    public Renderer Fog { get { return fog; } set { fog = value; } }
    public float Alpha { get; set; } = 0.0f;
    private Shader shader;
    //private bool isDoneLooping = false;

    // Start is called before the first frame update
    void Start()
    {
        //shader = Shader.Find("Shader Graphs/Fog");
        //fog.material.shader = shader;
        Fog.material.EnableKeyword("Vector1_100D317A");
    }

    // Update is called once per frame
    void Update()
    {

        Fog.material.SetFloat("Vector1_100D317A", Alpha);
        //testcode
        //if (!isDoneLooping)
        //{
        //    if (Alpha >= 1)
        //    {
        //        isDoneLooping = true;
        //    }
        //    else
        //    {
        //        Alpha = Alpha + 0.1f * Time.deltaTime;
        //    }

            
        //}
        //else if(isDoneLooping)
        //{
        //    if(Alpha <= 0)
        //    {
        //        isDoneLooping = false;
        //    }
        //    else
        //    {
        //        Alpha = Alpha - 0.1f * Time.deltaTime;

        //    }


        
    }

}


