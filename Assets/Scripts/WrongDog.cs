﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WrongDog : MonoBehaviour
{
    public SphereCollider SphereCollider { get; set; }
    public MeshRenderer FogWall { get; set; }
    public static bool IsBadDog { get; set; }
    public Collider FogCollider { get; set; }
    public GameObject FogChasing { get; set; }
    public FogSystem FogSystem { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        FogWall = transform.GetChild(0).GetComponent<MeshRenderer>();
        FogCollider = transform.GetChild(0).GetComponent<MeshCollider>();
        SphereCollider = GetComponent<SphereCollider>();
        FogWall.material.EnableKeyword("Vector1_100D317A");
        FogChasing = GameObject.FindGameObjectWithTag("Fog System");
        FogSystem = FogChasing.GetComponent<FogSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (IsBadDog)
        {
            FogWall.material.SetFloat("Vector1_100D317A", 1);
            FogSystem.Speed = 20;
            FogCollider.enabled = true;
        }
        else
        {
            if(FogWall.material.GetFloat("Vector1_100D317A") == 1 && FogCollider.enabled == true)
            {
                FogWall.material.SetFloat("Vector1_100D317A", 0);
                FogCollider.enabled = false;
                IsBadDog = false;
            }
            
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            IsBadDog = true;
        }
    }

}
