﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Events;

public class WhiteFlashTimeTravel : MonoBehaviour
{

    private RawImage RawImage { get; set; }
    private float counter = 0;
    public static bool PlayFadeToBlackBool { get; set; } = false;
    public static bool PlayFadeToWhiteBool { get; set; } = false;

    

    // Start is called before the first frame update
    void Start()
    {


        RawImage = GetComponent<RawImage>();
        InvokeRepeating("PlayFadeBlack", 0.01f, 0.01f);
        InvokeRepeating("PlayFadeWhite", 0.01f, 0.01f);
        



    }

    // Update is called once per frame
    void Update()
    {
        
            
    }

    public void WhiteFlash()
    {
        PlayFadeToWhiteBool = true;
    }

    private void OnDisable()
    {
        SimpleEventSystem.RemoveListener(Events.Types.TimeReversalPowerUpEnable, WhiteFlash);
        SimpleEventSystem.RemoveListener(Events.Types.TimeReversalPowerUpDisable, WhiteFlash);
    }

    private void OnEnable()
    {
        SimpleEventSystem.AddListener(Events.Types.TimeReversalPowerUpEnable, WhiteFlash);
        SimpleEventSystem.AddListener(Events.Types.TimeReversalPowerUpDisable, WhiteFlash);
    }


    private void PlayFadeBlack()
    {
        if (PlayFadeToBlackBool)
        {
            if (counter <= 1000)
            {
                float lerprate = counter / 1000;
                //float a = Mathf.Lerp(0, 256, lerprate);
                //int intA = Mathf.FloorToInt(a);

                RawImage.color = Color.Lerp(new Color(0, 0, 0, 0), Color.black, lerprate + 0.5f);
                //RawImage.color = Color.black;
                counter++;
            }
            else
            {
                if (counter <= 5000)
                {
                    RawImage.color = Color.black;
                    counter++;
                }
                else
                {
                    
                    PlayFadeToBlackBool = false;
                    counter = 0;
                    Application.Quit(1);
                }
                
                
            }

        }
    }


    private void PlayFadeWhite()
    {
        if (PlayFadeToWhiteBool && !PlayFadeToBlackBool)
        {
            if (counter <= 50 && !(counter >= 50))
            {
                float lerprate = counter / 50;
                //float a = Mathf.Lerp(0, 256, lerprate);
                //int intA = Mathf.FloorToInt(a);

                RawImage.color = Color.Lerp(new Color(0,0,0,0), Color.white, lerprate);
                //RawImage.color = Color.black;
                counter++;
            }
            else if (counter >= 50 && !(counter >= 100))
            {
                float lerprate = (counter - 50) / 50;
                float a = Mathf.Lerp(0, 256, lerprate);
                //int intA = Mathf.FloorToInt(a);

                RawImage.color = Color.Lerp(Color.white, new Color(0, 0, 0, 0), lerprate);
                //RawImage.color = Color.black;
                counter++;
            }
            else
            {
                PlayFadeToWhiteBool = false;
                RawImage.color = new Color(0, 0, 0, 0);
                counter = 0;
            }

        }
    }




}
