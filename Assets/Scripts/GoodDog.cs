﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoodDog : MonoBehaviour
{
    public SphereCollider SphereCollider { get; set; }
    public MeshRenderer FogWall { get; set; }
    public static bool IsGoodDog { get; set; }
    public Collider FogCollider { get; set; }


    // Start is called before the first frame update
    void Start()
    {
        SphereCollider = GetComponent<SphereCollider>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            IsGoodDog = true;
        }
    }
}
