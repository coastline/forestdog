using Events;
using Interactable;
using UnityEngine;

namespace Player
{
    public class Interactor : MonoBehaviour
    {
        private IInteractable _currentInteractable;
        private bool _interacting;

        private void Update()
        {
            if (ReferenceEquals(_currentInteractable, null)) return;

            bool interact = Input.GetButton("Interact");
            if (interact == _interacting) return;

            if (interact) _currentInteractable.StartInteracting();
            else _currentInteractable.StopInteracting();

            _interacting = interact;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!ReferenceEquals(_currentInteractable, null)) return;
            
            _currentInteractable = other.GetComponent<IInteractable>();
            if (ReferenceEquals(_currentInteractable, null)) return;

            SimpleEventSystem.TriggerEvent(Events.Types.InteractableDetected);
        }

        private void OnTriggerExit(Collider other)
        {
            if (ReferenceEquals(_currentInteractable, null)) return;
            
            IInteractable interactable = other.GetComponent<IInteractable>();
            if (!ReferenceEquals(interactable, _currentInteractable)) return;
            
            _currentInteractable.StopInteracting();
            _currentInteractable = null;
            _interacting = false;

            SimpleEventSystem.TriggerEvent(Events.Types.InteractableRemoved);
        }
    }
}