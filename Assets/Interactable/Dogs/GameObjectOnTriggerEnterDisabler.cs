using System;
using UnityEngine;

namespace Interactable.Dogs
{
    public class GameObjectOnTriggerEnterDisabler : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (!other.gameObject.CompareTag("Doggo")) return;

            other.gameObject.SetActive(false);
        }
    }
}