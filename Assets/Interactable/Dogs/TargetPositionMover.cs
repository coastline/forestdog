using System;
using UnityEngine;

namespace Interactable.Dogs
{
    public class TargetPositionMover : MonoBehaviour
    {
        public Transform target;
        public float time;

        private float _startTime;
        private Vector3 _startPosition;

        private Animator _animator;

        public void Enable()
        {
            enabled = true;
        }

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        private void Start()
        {
            enabled = false;
        }

        private void OnEnable()
        {
            _startTime = Time.time;
            _startPosition = transform.position;

            _animator.SetTrigger("Walk");
        }

        private void OnDisable()
        {
            _animator.ResetTrigger("Walk");
        }

        private void Update()
        {
            float delta = (Time.time - _startTime) / time;

            transform.position = Vector3.LerpUnclamped(_startPosition, target.position, delta);
        }
        
        
    }
}