using UnityEngine;

namespace Interactable.Dogs
{
    public class MonoBehaviourOnTriggerEnterEnabler : MonoBehaviour
    {
        public MonoBehaviour _behaviour;

        private void OnTriggerEnter(Collider other)
        {
            if (!other.gameObject.CompareTag("Player")) return;
            if (ReferenceEquals(_behaviour, null)) {
                Debug.LogError("mono behaviour cannot be null");
                return;
            }

            _behaviour.enabled = true;
        }
    }
}