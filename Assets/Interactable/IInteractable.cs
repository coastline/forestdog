namespace Interactable
{
    public interface IInteractable
    {
        void StartInteracting();
        void StopInteracting();
    }
}