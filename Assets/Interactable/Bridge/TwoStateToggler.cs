using Events;
using UnityEngine;

namespace Interactable.Bridge
{
    public class TwoStateToggler : MonoBehaviour
    {
        [SerializeField]
        private GameObject normalState;
        [SerializeField]
        private GameObject brokenState;

        private void OnEnable()
        {
            SimpleEventSystem.AddListener(Events.Types.TimeReversalPowerUpEnable, TriggerNormalState);
            SimpleEventSystem.AddListener(Events.Types.TimeReversalPowerUpDisable, TriggerBrokenState);
        }

        private void OnDisable()
        {
            SimpleEventSystem.RemoveListener(Events.Types.TimeReversalPowerUpEnable, TriggerNormalState);
            SimpleEventSystem.RemoveListener(Events.Types.TimeReversalPowerUpDisable, TriggerBrokenState);
        }

        private void TriggerNormalState()
        {
            brokenState.SetActive(false);
            normalState.SetActive(true);
        }

        private void TriggerBrokenState()
        {
            brokenState.SetActive(true);
            normalState.SetActive(false);
        }
    }
}