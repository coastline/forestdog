using System;
using UnityEngine;

namespace Interactable.Boulder
{
    public class TargetPositionFollower : MonoBehaviour
    {
        [SerializeField] private GameObject target = null;
        [SerializeField] private Vector3 difference = Vector3.zero;

        public void SetTarget(GameObject newTarget)
        {
            target = newTarget;

            if (!IsTargetNull()) difference = target.transform.position - transform.position;
            else difference = Vector3.zero;
        }

        private void FixedUpdate()
        {
            if (IsTargetNull()) return;

            transform.position = target.transform.position - difference;
        }

        private bool IsTargetNull()
        {
            return target == null || target.Equals(null);
        }
    }
}