using UnityEngine;

namespace Interactable.Gramophone
{
    public class GramphoneMusicPlayer2 : MonoBehaviour
    {
        [FMODUnity.EventRef]
        public string FmodEvent = "";
        FMOD.Studio.EventInstance gramophoneMusic;

        public void PlaySong()
        {
            gramophoneMusic = FMODUnity.RuntimeManager.CreateInstance(FmodEvent);
            gramophoneMusic.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform));
            gramophoneMusic.start();
            Debug.Log("MUSIC STARTS");
        }

        public void StopSong()
        {
            gramophoneMusic.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            Debug.Log("MUSIC ENDS");
        }
    }
}