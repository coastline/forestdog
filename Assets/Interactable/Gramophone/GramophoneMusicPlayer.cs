using Events;
using UnityEngine;
using FMODUnity;

namespace Interactable.Gramophone
{
    [RequireComponent(typeof(AudioSource))]
    public class GramophoneMusicPlayer : MonoBehaviour
    {
        [FMODUnity.EventRef]
        public string FmodEvent = "";
        FMOD.Studio.EventInstance gramophoneMusic;
              
        private void OnEnable()
        {          
            SimpleEventSystem.AddListener(Events.Types.TimeReversalPowerUpEnable, PlaySong);
            SimpleEventSystem.AddListener(Events.Types.TimeReversalPowerUpDisable, StopSong);
        }

        private void OnDisable()
        {
            SimpleEventSystem.RemoveListener(Events.Types.TimeReversalPowerUpEnable, PlaySong);
            SimpleEventSystem.RemoveListener(Events.Types.TimeReversalPowerUpDisable, StopSong);
        }

        private void PlaySong()
        {
            gramophoneMusic = FMODUnity.RuntimeManager.CreateInstance(FmodEvent);
            gramophoneMusic.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform));
            gramophoneMusic.start();
            Debug.Log("MUSIC STARTS");
        }

        private void StopSong()
        {
            gramophoneMusic.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            Debug.Log("MUSIC ENDS");
        }
    }
}