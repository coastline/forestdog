using Events;
using UnityEngine;

namespace Interactable
{
    public class InteractableUIActivator : MonoBehaviour
    {
        public GameObject ui;
        
        private void OnEnable()
        {
            SimpleEventSystem.AddListener(Events.Types.InteractableDetected, EnableUI);
            SimpleEventSystem.AddListener(Events.Types.InteractableRemoved, DisableUI);
        }

        private void OnDisable()
        {
            SimpleEventSystem.RemoveListener(Events.Types.InteractableDetected, EnableUI);
            SimpleEventSystem.RemoveListener(Events.Types.InteractableRemoved, DisableUI);
        }

        private void EnableUI()
        {
            ui.SetActive(true);
        }
        
        private void DisableUI()
        {
            ui.SetActive(false);
        }
    }
}