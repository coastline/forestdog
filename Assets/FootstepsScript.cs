﻿using System;
using Events;
using UnityEngine;
using FMODUnity;

    public class FootstepsScript : MonoBehaviour
    {
        [FMODUnity.EventRef]
        public string FmodEvent = "";
        FMOD.Studio.EventInstance footstepsSFX;

    private void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {

        }
    }

    private void PlaySteps()
        {
            footstepsSFX = FMODUnity.RuntimeManager.CreateInstance(FmodEvent);
            footstepsSFX.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform));
            footstepsSFX.start();          
        }

        private void StopSteps()
        {
            footstepsSFX.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }
    }